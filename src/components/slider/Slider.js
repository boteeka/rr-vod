import React, { useReducer } from 'react';
import ArrowBackIosIcon from '@material-ui/icons/ArrowBackIos';
import ArrowForwardIosIcon from '@material-ui/icons/ArrowForwardIos';
import { Hidden } from '@material-ui/core';

const createSwipeHandlers = (
	leftCb = () => {},
	rightCb = () => {},
	upCb = () => {},
	downCb = () => {},
) => {
	const minDelta = 10;
	let x = 0;
	let y = 0;

	const onTouchStartHandler = e => {
		const touch = e.touches[0];
		x = touch.clientX;
		y = touch.clientY;
	};

	const onTouchMoveHandler = e => {
		const touch = e.touches[0];
		const deltaX = touch.clientX - x;
		const deltaY = touch.clientY - y;
		const absX = Math.abs(deltaX);
		const absY = Math.abs(deltaY);

		x = touch.clientX;
		y = touch.clientY;

		if (absX < minDelta && absY < minDelta) {
			return;
		}

		if (absX > absY) {
			if (deltaX < 0) {
				leftCb();
			} else {
				rightCb();
			}
		} else {
			if (deltaY < 0) {
				upCb();
			} else {
				downCb();
			}
		}
	};

	return [onTouchStartHandler, onTouchMoveHandler];
};

// The main component
const Slider = props => {
	const initialState = {
		direction: null,
		position: 0,
		isSliding: false,
		slideEnded: true,
		slideCount: 0,
	};

	const [state, dispatch] = useReducer((state, action) => {
		switch (action.type) {
			case 'next':
				return {
					...state,
					direction: 'next',
					position: action.position,
				};
			case 'prev':
				return {
					...state,
					direction: 'prev',
					position: action.position,
				};
			case 'end-sliding':
				return {
					...state,
					slideEnded: true,
				};
			case 'start-sliding':
				return state.slideEnded
					? {
							...state,
							isSliding: true,
							slideEnded: false,
					  }
					: state;
			case 'stop-sliding':
				return {
					...state,
					isSliding: false,
				};

			default:
				return state;
		}
	}, initialState);

	const slides = React.Children.toArray(props.children);

	const calculateOrder = (index, position, slides) => {
		const numItems = slides.length || 1;
		if (index - position < 0) {
			return numItems - Math.abs(index - position) + 1;
		}

		return index - position + 1;
	};

	const renderSlides = slides => {
		return slides.map((slide, index) => (
			<Slide
				key={index}
				order={calculateOrder(index, state.position, slides)}
				{...props}
			>
				{slide.props.children}
			</Slide>
		));
	};

	// handle the animation driving logic
	const doSlide = () => {
		dispatch({ type: 'start-sliding' });

		setTimeout(() => {
			dispatch({ type: 'stop-sliding' });
		}, 50);

		// signal that that sliding is possible again
		setTimeout(() => {
			dispatch({ type: 'end-sliding' });
		}, 1000);
	};

	const slideNext = () => {
		if (!state.slideEnded) return;
		dispatch({
			type: 'next',
			position:
				state.position === slides.length - 1 ? 0 : state.position + 1,
		});
		doSlide();
	};

	const slidePrev = () => {
		if (!state.slideEnded) return;
		dispatch({
			type: 'prev',
			position:
				state.position === 0 ? slides.length - 1 : state.position - 1,
		});
		doSlide();
	};

	const [onTouchStartHandler, onTouchMoveHandler] = createSwipeHandlers(
		slideNext,
		slidePrev,
	);

	return (
		<div
			{...props}
			style={{ overflow: 'hidden', position: 'relative' }}
			onTouchStart={onTouchStartHandler}
			onTouchMove={onTouchMoveHandler}
		>
			<Controls next={slideNext} prev={slidePrev} />
			<SlideDeck
				sliding={state.isSliding}
				direction={state.direction}
				numSlides={slides.length}
			>
				{renderSlides(slides)}
			</SlideDeck>
		</div>
	);
};

// Component representing one single slide
const Slide = props => {
	const styles = {
		flex: '1 0 100%',
		flexBasis: '100%',
		order: props.order,
	};

	return (
		<li {...props} style={styles}>
			<div>{props.children}</div>
		</li>
	);
};

// Component representing the slide deck
const SlideDeck = props => {
	const calculateTransform = props => {
		if (props.numSlides === 1) {
			return 'translateX(0%)';
		}

		if (props.numSlides === 2) {
			if (!props.sliding && props.direction === 'next') {
				return 'translateX(calc(-100%))';
			}

			if (!props.sliding && props.direction === 'prev') {
				return 'translateX(0%)';
			}

			if (props.direction === 'prev') {
				return 'translateX(calc(-100%))';
			}

			return 'translateX(0%)';
		}

		if (!props.sliding) {
			return 'translateX(calc(-100%))';
		}

		if (props.direction === 'prev') {
			return 'translateX(calc(2 * (-100%)))';
		}

		return 'translateX(0%)';
	};

	const styles = {
		width: '100%',
		listStyle: 'none',
		margin: 0,
		padding: 0,
		display: 'flex',
		transition: props.sliding ? 'none' : 'transform 1s ease',
		transform: calculateTransform(props),
	};

	return <ul style={styles}>{props.children}</ul>;
};

const Controls = props => {
	return (
		<Hidden smDown>
			<div
				style={{
					position: 'absolute',
					left: 0,
					top: 0,
					bottom: 0,
					paddingTop: '16%',
					zIndex: 2,
					cursor: 'pointer',
					textAlign: 'right',
				}}
				onClick={props.prev}
			>
				<ArrowBackIosIcon
					color='action'
					fontSize='large'
					style={{ marginLeft: '12px' }}
				/>
			</div>

			<div
				style={{
					position: 'absolute',
					right: 0,
					top: 0,
					bottom: 0,
					paddingTop: '16%',
					zIndex: 2,
					cursor: 'pointer',
					textAlign: 'left',
				}}
				onClick={props.next}
			>
				<ArrowForwardIosIcon
					color='action'
					fontSize='large'
					tyle={{ marginRight: '5px' }}
				/>
			</div>
		</Hidden>
	);
};

export { Slider };
