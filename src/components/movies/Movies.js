import React, { Component } from "react";
import { connect } from "react-redux";
import { fetchMovies } from "../../actions/movies-actions";
import { Waypoint } from "react-waypoint";
import Spinner from "../spinner/Spinner";
import { Typography } from "@material-ui/core";
import Grid from "@material-ui/core/Grid";
import { fetchCategories } from "../../actions/category-actions";
import Movie from "./Movie";

class Movies extends Component {
    render () {
        const { error, loading, itemIds, itemsById, categories } = this.props;

        let categoryName;

        if (this.category_id && categories && categories.itemIds && categories.itemIds.length) {
            categoryName = categories.itemsById[this.category_id].name || '';
        }

        return (
            <div style={{ margin: "3vw 3vw 3vw 3vw" }}>
                <Typography gutterBottom variant="h5">{categoryName}</Typography>
                <Grid container spacing={32}>
                    {itemIds.map(id => (
                        <Grid key={id} item lg={3} sm={6} xs={12}>
                            <Movie movie={itemsById[id]}/>
                        </Grid>
                    ))}
                </Grid>
                {!loading && (<Waypoint
                    key={this.category_id + "wp"}
                    onEnter={this.handleWaypointEnter}
                />)}
                {error && <div>error</div>}
                {loading && (<Grid container justify="center" alignItems="center">
                    <Grid item lg={1}>
                        <Spinner/>
                    </Grid>
                </Grid>)}
            </div>
        );
    }

    loadPage = page => {
        this.props.dispatch(fetchMovies(this.category_id, page));
    };

    componentDidMount () {
        const {
                  match: {
                             params: { category_id }
                         }
              }          = this.props;
        this.category_id = category_id;
        this.props.dispatch(fetchCategories());
        this.loadPage(1);

    }

    handleWaypointEnter = () => {
        const { pagesLoaded } = this.props;
        const nextPage        = pagesLoaded.length > 0 ? Math.max(...pagesLoaded) + 1 : 1;
        this.loadPage(nextPage);
    };
}

const mapStateToProps = function (state) {
    return {
        ...state.movies,
        categories: {
            ...state.categories
        }
    };
};

export default connect(mapStateToProps)(Movies);
