import React, { Component } from "react";
import { Link } from "react-router-dom";
import { Card, CardMedia, Typography } from "@material-ui/core";
import CardActionArea from "@material-ui/core/CardActionArea";

class Movie extends Component {
    render () {
        const { id, title, poster_path, backdrop_path } = this.props.movie;

        return (
            <Link to={`/asset/${id}`} style={{ textDecoration: 'none' }}>
                <Card elevation={16}>
                    <CardActionArea>
                        <CardMedia image={backdrop_path || poster_path} style={{ height: 200 }}>
                            <Typography variant="subtitle1" style={{
                                position:   "absolute",
                                bottom:     "1em",
                                left:       "1em",
                                textShadow: "-1px -1px 0 rgba(0, 0, 0, 0.4), 1px -1px 0 rgba(0, 0, 0, 0.4), -1px 1px 0 rgba(0, 0, 0, 0.4), 1px 1px 0 rgba(0, 0, 0, 0.4)"
                            }}>
                                {title}
                            </Typography>
                        </CardMedia>
                    </CardActionArea>
                </Card>
            </Link>
        );
    }
}

export default Movie;
