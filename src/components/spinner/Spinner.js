import CircularProgress from '@material-ui/core/CircularProgress';
import React from "react";
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';


const styles = theme => ({
    progress:  {
        margin: theme.spacing.unit * 4,
    },
    container: {
        padding: theme.spacing.unit * 3
    },
});

function Spinner (props) {
    const { classes } = props;
    return (
        <div className={classes.container}>
            <CircularProgress color="secondary" className={classes.progress}/>
        </div>
    );
}

Spinner.propTypes = {
    classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(Spinner);
