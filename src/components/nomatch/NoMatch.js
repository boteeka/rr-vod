import React, { Component } from "react";
import { Typography } from "@material-ui/core";
import Grid from "@material-ui/core/Grid";
import { fetchPopularAssets } from "../../actions/popular-assets-actions";
import { connect } from "react-redux";
import Movie from "../movies/Movie";
import Spinner from "../spinner/Spinner";

class NoMatch extends Component {
    render () {
        const { loading, itemIds, itemsById } = this.props;

        return (
            <React.Fragment>
                <div style={{ margin: "5vw 5vw 0 5vw" }}>
                    <Typography gutterBottom variant="h5">Oops! Page not found!</Typography>
                    <Grid container spacing={32} alignItems="center">
                        <Grid item lg={10} sm={12} xs={12}>
                            <Typography variant="h1" gutterBottom>Error 404</Typography>
                            <Typography variant="body2" color="textSecondary">The path "{window.location.href}" could
                                not be found.</Typography>
                        </Grid>
                    </Grid>

                </div>


                <div style={{ margin: "3vw 3vw 3vw 3vw" }}>
                    <Typography gutterBottom variant="h5">You might want to check out some of our popular movies:</Typography>
                    <Grid container spacing={32}>
                        {itemIds.slice(-3).map(id => (
                            <Grid key={id} item lg={4} sm={6} xs={12}>
                                <Movie movie={itemsById[id]}/>
                            </Grid>
                        ))}
                    </Grid>

                    {loading && (<Grid container justify="center" alignItems="center">
                        <Grid item lg={1}>
                            <Spinner/>
                        </Grid>
                    </Grid>)}
                </div>
            </React.Fragment>
        )
    }

    componentDidMount () {
        this.props.dispatch(fetchPopularAssets());
    }
}

const mapStateToProps = function (state) {
    return state.popular;
};

export default connect(mapStateToProps)(NoMatch);
