import React, { Component } from 'react';
import Grid from '@material-ui/core/Grid';
import Spinner from '../spinner/Spinner';
import { connect } from 'react-redux';
import { fetchPopularAssets } from '../../actions/popular-assets-actions';
// import Carousel from '../carousel/Carousel';
import { AspectRatio } from 'react-aspect-ratio';
import 'react-aspect-ratio/aspect-ratio.css';
import { Typography } from '@material-ui/core';
import { NavLink } from 'react-router-dom';
import { Slider } from '../slider/Slider';

class Home extends Component {
	render() {
		const { error, loading, itemIds, itemsById } = this.props;

		if (error) {
			return null;
		}

		return (
			<div
				style={{
					margin: '3vw 3vw 3vw 3vw',
					borderRadius: 3,
					overflow: 'hidden',
				}}
			>
				<Typography gutterBottom variant='h4'>
					Welcome to Yet Another Movie Database
				</Typography>
				<Slider>
					{itemIds.map(id => (
						<div key={id}>
							<div
								style={{
									position: 'relative',
								}}
							>
								<AspectRatio
									style={{ maxWidth: '22vw' }}
									ratio='500/750'
								>
									<img
										alt=''
										src={itemsById[id].poster_path}
									/>
								</AspectRatio>
								<NavLink to={`/asset/${id}`}>
									<Typography
										gutterBottom
										variant='h3'
										style={{
											position: 'absolute',
											bottom: '.1em',
											right: '1em',
											left: '25vw',
											textAlign: 'right',
											fontSize: '3vw',
										}}
									>
										{itemsById[id].title}
									</Typography>
								</NavLink>
								<div
									style={{
										backgroundImage: `url(${itemsById[id]
											.backdrop_path || ''})`,
										backgroundRepeat: 'no-repeat',
										backgroundSize: 'cover',
										backgroundPosition: '10vw center',
										position: 'absolute',
										top: 0,
										left: 0,
										right: 0,
										bottom: 0,
										opacity: 0.6,
										zIndex: -1,
									}}
								/>
							</div>
						</div>
					))}					
				</Slider>
				
				{loading && (
					<Grid container justify='center' alignItems='center'>
						<Grid item lg={1}>
							<Spinner />
						</Grid>
					</Grid>
				)}

				{!loading && (
					<Typography
						gutterBottom
						variant='body1'
						style={{ textAlign: 'center' }}
					>
						<br />
						<br />
						Here you will find information on a wide array of movie
						selection. You can watch trailers, find out the
						synopsis, etc.
						<br />
						Enjoy!
					</Typography>
				)}
			</div>
		);
	}

	componentDidMount() {
		this.props.dispatch(fetchPopularAssets());
	}
}

const mapStateToProps = function(state) {
	return state.popular;
};

export default connect(mapStateToProps)(Home);
