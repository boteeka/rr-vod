import React, { Component } from "react";
import { connect } from "react-redux";
import { withStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import Menu from "../menu/Menu";
import { Hidden } from "@material-ui/core";
import DrawerMenu from "../menu/DrawerMenu";


const styles = {
    root: {
        flexGrow: 1,
    },
    grow: {
        flexGrow: 1,
    },
};

class Header extends Component {

    constructor (props) {
        super(props);

        this.state = {
            drawerOpen: false
        };
    }

    toggleDrawer = () => {
        this.setState((prevState, props) => (
            {
                ...prevState,
                drawerOpen: !prevState.drawerOpen
            }
        ));
    };

    render () {
        const { loading } = this.props.menu;

        return (
            <React.Fragment>
                <AppBar position="sticky">
                    <Toolbar>
                        <Hidden mdUp>
                            <IconButton color="inherit" aria-label="Menu" onClick={this.toggleDrawer}>
                                <MenuIcon/>
                            </IconButton>
                        </Hidden>
                        <Typography variant="h6" color="inherit" style={{ marginRight: '3em' }}>
                            {this.props.title}
                        </Typography>
                        <Hidden smDown>
                            {!loading && <Menu menu={this.props.menu}/>}
                        </Hidden>

                    </Toolbar>
                </AppBar>
                {!loading && <DrawerMenu menu={this.props.menu} toggle={this.toggleDrawer} state={this.state}/>}
            </React.Fragment>
        );
    }


}

const mapStateToProps = function (state) {
    return state.header;
};

export default withStyles(styles)(connect(mapStateToProps)(Header));
