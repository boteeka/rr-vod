import React, { Component, useState } from "react";
import ItemContainer from "./ItemContainer";
import CarouselItem from "./CarouselItem";
import Wrapper from "./Wrapper";
import * as PropTypes from "prop-types";
import { Swipeable } from "react-swipeable";
import Controls from "./Controls";

class Carousel extends Component {
    constructor (props) {
        super(props);
        this.state = {
            position:  0,
            sliding:   false,
            direction: 'next',
        };
    }

    handleSwipe = (isNext) => {
        if (isNext) {
            this.nextSlide();
        } else {
            this.prevSlide();
        }
    };

    getOrder (itemIndex) {
        const { position } = this.state;
        const { children } = this.props;
        const numItems     = children.length || 1;
        if (itemIndex - position < 0) {
            return numItems - Math.abs(itemIndex - position)
        }
        return itemIndex - position;
    }

    nextSlide = () => {
        const { position } = this.state;
        const { children } = this.props;
        const numItems     = children.length || 1;
        this.doSliding('next', position === numItems - 1 ? 0 : position + 1);
    };

    prevSlide = () => {
        const { position } = this.state;
        const { children } = this.props;
        const numItems     = children.length || 1;
        this.doSliding('prev', position === 0 ? numItems - 1 : position - 1);
    };

    doSliding = (direction, position) => {
        this.setState({
            sliding: true,
            position,
            direction
        });
        setTimeout(() => {
            this.setState({
                sliding: false
            })
        }, 50);
    };

    render () {
        const { children } = this.props;

        return (
            <div style={{
                position: 'relative'
            }}>
                <Controls next={this.nextSlide} prev={this.prevSlide}/>
                <Swipeable

                    onSwipedLeft={() => this.handleSwipe(true)}
                    onSwipedRight={() => this.handleSwipe()}
                >
                    <Wrapper>
                        <ItemContainer sliding={this.state.sliding} direction={this.state.direction}>
                            {
                                children.map((child, index) => (
                                    <CarouselItem
                                        key={index}
                                        order={this.getOrder(index)}
                                    >
                                        {child}
                                    </CarouselItem>
                                ))
                            }
                        </ItemContainer>
                    </Wrapper>
                </Swipeable>
                {/*<button onClick={() => this.nextSlide()}>Next</button>*/}
                {/*<button onClick={() => this.prevSlide()}>Prev</button>*/}
            </div>
        )
    }
}

Carousel.propTypes = {
    children: PropTypes.node
};

export default Carousel;
