import React, { Component } from "react";
import * as PropTypes from "prop-types";
import ArrowBackIosIcon from '@material-ui/icons/ArrowBackIos';
import ArrowForwardIosIcon from '@material-ui/icons/ArrowForwardIos';
import { Hidden } from "@material-ui/core";

class Controls extends Component {
    render () {
        const { prev, next } = this.props;

        return (
            <Hidden smDown>
                <div style={{
                    position: 'absolute',
                    left: 0,
                    top: 0,
                    bottom: 0,
                    paddingTop: '16%',
                    zIndex: 2,
                    cursor: 'pointer',
                    textAlign: 'right'
                }}
                onClick={prev}>
                    <ArrowBackIosIcon color="action" fontSize="large" style={{marginLeft: '12px'}} />
                </div>

                <div style={{
                    position: 'absolute',
                    right: 0,
                    top: 0,
                    bottom: 0,
                    paddingTop: '16%',
                    zIndex: 2,
                    cursor: 'pointer',
                    textAlign: 'left'
                }}
                onClick={next}>
                    <ArrowForwardIosIcon color="action" fontSize="large" tyle={{marginRight: '5px'}} />
                </div>
            </Hidden>

        );
    }
}

Controls.propTypes = {
    prev: PropTypes.func,
    next: PropTypes.func
};

export default Controls;
