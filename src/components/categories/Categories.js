import React, { Component } from "react";
import { fetchCategories } from "../../actions/category-actions";
import { connect } from "react-redux";
import { Link } from "react-router-dom";
import { Typography } from "@material-ui/core";
import Grid from "@material-ui/core/Grid";
import Spinner from "../spinner/Spinner";
import Button from "@material-ui/core/Button";

class Categories extends Component {
    render () {
        const { error, loading, itemIds, itemsById } = this.props;

        return (
            <div style={{ margin: "3vw 3vw 3vw 3vw" }}>
                <Typography gutterBottom variant="h5">Genres</Typography>
                <Grid container spacing={32}>
                    {itemIds.map(id => (
                        <Grid key={id} item lg={3} sm={6} xs={12}>
                            <Button component={Link} to={`/movies/${id}`} variant="outlined" size="large"
                                    style={{ display: 'block' }}>
                                <Typography variant="h6">{itemsById[id].name}</Typography>
                            </Button>
                        </Grid>
                    ))}
                </Grid>
                {error && <div>error</div>}
                {loading && (<Grid container justify="center" alignItems="center">
                    <Grid item lg={1}>
                        <Spinner/>
                    </Grid>
                </Grid>)}
            </div>
        )
    }

    componentDidMount () {
        this.props.dispatch(fetchCategories());
    }
}

const mapStateToProps = function (state) {
    return state.categories;
};


export default connect(mapStateToProps)(Categories);
