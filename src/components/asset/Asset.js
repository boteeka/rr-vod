import React, { Component } from "react";
import Grid from "@material-ui/core/Grid";
import Spinner from "../spinner/Spinner";
import { connect } from "react-redux";
import { fetchAsset } from "../../actions/asset-actions";
import { Collapse, Dialog, DialogContent, DialogTitle, Typography } from "@material-ui/core";
import 'react-aspect-ratio/aspect-ratio.css';
import { AspectRatio } from "react-aspect-ratio";
import { NavLink } from "react-router-dom";
import StarIcon from '@material-ui/icons/Star';
import { fetchAssetvideos } from "../../actions/asset-videos-actions";
import Button from "@material-ui/core/Button";
import DialogActions from "@material-ui/core/DialogActions";
import YouTube from "react-youtube";

class Asset extends Component {
    static defaultProps = {
        error:       false,
        loading:     true,
        itemsById:   {},
        assetVideos: {
            itemsById: {}
        }
    };

    constructor (props) {
        super(props);

        this.state = {
            dialogOpen:      false,
            currentTrailer:  null,
            trailersVisible: false
        };
    }

    render () {

        const { error, loading, itemsById, assetVideos } = this.props;

// || !itemsById || !this.asset_id || !itemsById[this.asset_id] || !assetVideos || !assetVideos.itemsById ||
// assetVideos.itemsById[this.asset_id]
        if (loading || !itemsById || !this.asset_id || !itemsById[this.asset_id]) {
            return (
                <Grid container justify="center" alignItems="center">
                    <Grid item lg={1}>
                        <Spinner/>
                    </Grid>
                </Grid>
            );
        }

        const asset  = itemsById[this.asset_id];
        const videos = assetVideos.itemsById[this.asset_id];

        const styles = {
            position:           'absolute',
            top:                0,
            left:               0,
            right:              0,
            bottom:             0,
            backgroundImage:    `url(${asset.backdrop_path || ''})`,
            backgroundSize:     'cover',
            backgroundRepeat:   'no-repeat',
            backgroundPosition: 'center',
            opacity:            0.2,
            zIndex:             -1,
            boxShadow:          'inset 0 0 50px rgba(0,0,0,0.6)'
        };

        return (
            <div style={{ margin: "3vw 3vw 3vw 3vw", position: 'relative', borderRadius: 3, overflow: 'hidden' }}>
                <Grid container>
                    <Grid item lg={4} md={4} sm={12} xs={12}>
                        <AspectRatio ratio="500/750">
                            <img alt={asset.title} src={asset.poster_path}/>
                        </AspectRatio>
                    </Grid>
                    <Grid item lg={8} md={8} sm={12} xs={12}>
                        <div style={{ padding: '1vw', paddingBottom: '5vw' }}>
                            <Typography gutterBottom variant="h3">
                                {asset.title}
                            </Typography>

                            {asset.tagline && <Typography gutterBottom variant="subtitle1" color="textSecondary">
                                "{asset.tagline}"
                            </Typography>}
                            <br/>
                            <Typography gutterBottom variant="caption" color="textSecondary">
                                <StarIcon color="action" fontSize="small" style={{ verticalAlign: 'bottom' }}/> <span
                                title="Rating average">{asset.vote_average}/10</span> <span
                                title="Votes casted">({asset.vote_count})</span>
                            </Typography>

                            <Typography gutterBottom variant="caption" color="textSecondary">
                                Year of release: {(new Date(asset.release_date)).getFullYear()} ({asset.status})
                            </Typography>
                            <Typography gutterBottom variant="caption" color="textSecondary">
                                Runtime: {asset.runtime} minutes
                            </Typography>
                            <Typography gutterBottom variant="caption" color="textSecondary">
                                Languages: {
                                asset.spoken_languages.map((lang, index) =>
                                    <span key={lang.name}>{lang.name}{((asset.spoken_languages.length - 1) !== index) &&
                                    <span>, </span>}</span>
                                )
                            }
                            </Typography>
                            <Typography gutterBottom variant="caption" color="textSecondary">
                                Genres: {
                                asset.genres.map((genre, index) =>
                                    <NavLink key={genre.id} title={genre.name} to={'/movies/' + genre.id} style={{
                                        textDecoration: 'none',
                                        color:          'inherit'
                                    }}><span>{genre.name}</span>{((asset.genres.length - 1) !== index) &&
                                    <span>, </span>}</NavLink>
                                )
                            }
                            </Typography>
                            <Typography gutterBottom variant="caption" color="textSecondary">
                                <a title={asset.title + ' on IMDB'} rel="noopener noreferrer"
                                   href={"https://www.imdb.com/title/" + asset.imdb_id}
                                   target='_blank' style={{ textDecoration: 'none', color: 'inherit' }}>IMDB</a>
                                <br/>
                                <a title={asset.homepage} rel="noopener noreferrer"
                                   href={asset.homepage}
                                   target='_blank' style={{ textDecoration: 'none', color: 'inherit' }}>Movie
                                    Homepage</a>
                            </Typography>
                            <br/>
                            <Typography gutterBottom variant="body1">{asset.overview}</Typography>
                            {videos.results.length && <div>
                                <Button onClick={this.handleTrailersVisibility} variant="outlined">Watch
                                    trailers</Button>
                                <br/>
                                <br/>
                                <br/>

                                <Collapse in={this.state.trailersVisible}>
                                    <Grid container spacing={16}>
                                        {
                                            videos.results.map(
                                                video => <Grid item lg={2} md={4} sm={12} xs={12} key={video.key}>
                                                    <AspectRatio ratio="480/360">
                                                        <img onClick={(e) => this.handleTrailerClick(video, e)}
                                                             src={'https://img.youtube.com/vi/' + video.key + '/0.jpg'}
                                                             alt={video.name}
                                                             style={{
                                                                 cursor: 'pointer'
                                                             }}/>
                                                    </AspectRatio>
                                                </Grid>)
                                        }
                                    </Grid>
                                </Collapse>
                            </div>}

                            <Dialog open={this.state.dialogOpen} maxWidth="lg" fullWidth={true}
                                    onBackdropClick={this.handleDialogBackdropClick}>
                                <DialogTitle>{this.state.currentTrailer && this.state.currentTrailer.name}</DialogTitle>
                                <DialogContent>
                                    <AspectRatio ratio="16/9">
                                        <YouTube videoId={this.state.currentTrailer && this.state.currentTrailer.key}
                                                 opts={{
                                                     height: '100%',
                                                     width:  '100%',
                                                 }}/>
                                    </AspectRatio>
                                </DialogContent>
                                <DialogActions>
                                    <Button onClick={this.handleDialogClose} color="secondary">
                                        Close
                                    </Button>
                                </DialogActions>
                            </Dialog>


                        </div>

                        <div style={{
                            textAlign:       'right',
                            backgroundImage: 'linear-gradient(transparent, rgba(0,0,0,0.4))',
                            position:        'absolute',
                            left:            0,
                            right:           0,
                            bottom:          0
                        }}>
                            {
                                asset.production_companies.map((pc, index) =>
                                    (pc.logo_path &&
                                        <img key={pc.id} src={pc.logo_path} alt={pc.name} title={pc.name}
                                             style={{
                                                 maxWidth:      200,
                                                 maxHeight:     '5vw',
                                                 margin:        10,
                                                 verticalAlign: 'middle'
                                             }}/>)
                                )
                            }
                        </div>

                    </Grid>
                </Grid>


                {error && <div>error</div>}
                <div style={styles}/>
            </div>
        )
    }

    handleDialogBackdropClick = () => {
        this.setState((prevState, props) => ({
            ...prevState,
            dialogOpen: false
        }));
    };

    handleTrailersVisibility = () => {
        this.setState((prevState, props) => ({
            ...prevState,
            trailersVisible: !prevState.trailersVisible,
        }));
    };

    handleTrailerClick = (trailer, e) => {
        this.setState((prevState, props) => ({
            ...prevState,
            currentTrailer: trailer,
            dialogOpen:     true
        }));
    };

    handleDialogClose = () => {
        this.setState((prevState, props) => ({
            ...prevState,
            dialogOpen: false
        }));
    };

    componentDidMount () {
        const { match: { params: { id: asset_id } } } = this.props;
        this.asset_id                                 = asset_id;
        this.props.dispatch(fetchAsset(asset_id));
        this.props.dispatch(fetchAssetvideos(asset_id));
    }
}

const mapStateToProps = function (state) {
    return {
        ...state.assets,
        assetVideos: {
            ...state.assetVideos
        },
        loading:     state.loading || state.assetVideos.loading
    };
};

export default connect(mapStateToProps)(Asset);
