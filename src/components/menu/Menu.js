import React, { Component } from "react";
import { NavLink } from "react-router-dom";
import Button from "@material-ui/core/Button";


class Menu extends Component {
    render () {
        const { itemIds, itemsById } = this.props.menu;
        return (
            itemIds.map(id =>
                <Button component={NavLink} color="inherit" key={id} to={itemsById[id].route}>
                    {itemsById[id].label}
                </Button>
            )
        );
    }
}

export default Menu;
