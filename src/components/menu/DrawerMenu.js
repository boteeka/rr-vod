import React, { Component } from "react";
import { Link } from "react-router-dom";
import Button from "@material-ui/core/Button";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import { ListItemText } from "@material-ui/core";
import Typography from "@material-ui/core/Typography";
import SwipeableDrawer from "@material-ui/core/SwipeableDrawer";


class DrawerMenu extends Component {
    render () {
        const { toggle, state, menu: { itemIds, itemsById } } = this.props;

        return (
            <SwipeableDrawer
                anchor="top"
                open={state.drawerOpen}
                onClose={toggle}
                onOpen={toggle}>
                <List style={{ width: 'auto' }} onClick={toggle}>
                    <ListItem color="textSecondary" button>
                        <ListItemText>
                            <Typography variant="h6">Menu</Typography>
                        </ListItemText>
                    </ListItem>
                    {itemIds.map(id =>
                        <ListItem color="textSecondary" button key={itemsById[id].label}>
                            <ListItemText>
                                <Button component={Link} to={itemsById[id].route}
                                        variant="outlined" size="large"
                                        style={{ display: 'block' }}>
                                    <Typography variant="h6">{itemsById[id].label}</Typography>
                                </Button>
                            </ListItemText>
                        </ListItem>
                    )}
                </List>
            </SwipeableDrawer>
        );
    }
}

export default DrawerMenu;
