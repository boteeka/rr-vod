import React, { Component } from "react";
import { fetchPopularAssets } from "../../actions/popular-assets-actions";
import { connect } from "react-redux";
import Typography from "@material-ui/core/Typography";
import Grid from "@material-ui/core/Grid";
import Spinner from "../spinner/Spinner";
import Movie from "../movies/Movie";

class Popular extends Component {
    render () {
        const { error, loading, itemIds, itemsById } = this.props;

        return (
            <div style={{ margin: "3vw 3vw 3vw 3vw" }}>
                <Typography gutterBottom variant="h5">Popular</Typography>
                <Grid container spacing={32}>
                    {itemIds.map(id => (
                        <Grid key={id} item lg={3} sm={6} xs={12}>
                            <Movie movie={itemsById[id]}/>
                        </Grid>
                    ))}
                </Grid>
                {error && <div>error</div>}
                {loading && (<Grid container justify="center" alignItems="center">
                    <Grid item lg={1}>
                        <Spinner/>
                    </Grid>
                </Grid>)}
            </div>
        )
    }

    componentDidMount () {
        this.props.dispatch(fetchPopularAssets());
    }

}

const mapStateToProps = function (state) {
    return state.popular;
};

export default connect(mapStateToProps)(Popular);

