import { combineReducers } from "redux";
import headerReducer from './header-reducer';
import categoryReducer from './category-reducer';
import assetReducer from './asset-reducer';
import popularAssetsReducer from './popular-assets-reducer';
import moviesReducer from "./movies-reducer";
import assetvideoReducer from "./asset-videos-reducer";


export const allReducers = combineReducers({
    header:      headerReducer,
    categories:  categoryReducer,
    assets:      assetReducer,
    popular:     popularAssetsReducer,
    movies:      moviesReducer,
    assetVideos: assetvideoReducer
});

