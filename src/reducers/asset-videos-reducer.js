import {
    ASSETVIDEO_DATA_ERROR_ACTION,
    ASSETVIDEO_DATA_RECEIVED_ACTION,
    GET_ASSETVIDEO_DATA_ACTION
} from "../actions/asset-videos-actions";


const initialState = {
    itemIds:   [],
    itemsById: null,
    loading:   true,
    error:     null
};

const assetvideoReducer = (state = initialState, action) => {
    switch (action.type) {
        case GET_ASSETVIDEO_DATA_ACTION:
            return {
                ...state,
                loading: true,
                error:   null
            };
        case ASSETVIDEO_DATA_RECEIVED_ACTION:
            return {
                ...state,
                loading:   false,
                itemIds:   [...state.itemIds, action.payload.assetvideoData.id],
                itemsById: {
                    ...state.itemsById,
                    [action.payload.assetvideoData.id]: action.payload.assetvideoData
                }

            };
        case ASSETVIDEO_DATA_ERROR_ACTION:
            return {
                ...state,
                loading: false,
                error:   action.payload.error,
            };
        default:
            return state;

    }
};

export default assetvideoReducer;
