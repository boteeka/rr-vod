import {
    GET_MOVIES_DATA_ACTION,
    MOVIES_DATA_ERROR_ACTION,
    MOVIES_DATA_RECEIVED_ACTION
} from "../actions/movies-actions";

const initialState  = {
    category_id:   null,
    itemIds:       [],
    itemsById:     null,
    pagesLoaded:   [],
    loading:       true,
    error:         null,
    hasMore:       true,
};
const moviesReducer = (state = initialState, action) => {
    let itemIds     = state.itemIds;
    let itemsById   = state.itemsById;
    let pagesLoaded = state.pagesLoaded;


    switch (action.type) {
        case GET_MOVIES_DATA_ACTION:
            const categoryDidChange = state.category_id !== action.payload.category_id;

            if (categoryDidChange) {
                itemIds     = [];
                itemsById   = null;
                pagesLoaded = [];
            }

            return {
                ...state,
                loading:     true,
                error:       null,
                category_id: action.payload.category_id,
                itemIds,
                itemsById,
                pagesLoaded
            };
        case MOVIES_DATA_RECEIVED_ACTION:
            itemIds     = [
                ...(new Set([
                    ...state.itemIds,
                    ...action.payload.moviesData.results.map(item => item.id)
                ]))
            ];
            itemsById   = {
                ...state.itemsById,
                ...action.payload.moviesData.results.reduce((carry, element) => {
                    carry[element.id] = {
                        ...element,
                        poster_path:   element.poster_path ? 'https://image.tmdb.org/t/p/w500' + element.poster_path : null,
                        backdrop_path: element.backdrop_path ? 'https://image.tmdb.org/t/p/w780' + element.backdrop_path : null
                    };
                    return carry;
                }, {})
            };
            pagesLoaded = [...(new Set(state.pagesLoaded).add(action.payload.moviesData.page))];

            return {
                ...state,
                category_id:   action.payload.category_id,
                loading:       false,
                itemIds,
                itemsById,
                pagesLoaded,
                hasMore:       action.payload.moviesData.page < action.payload.moviesData.total_pages
            };
        case MOVIES_DATA_ERROR_ACTION:
            return {
                ...state,
                loading: false,
                error:   action.payload.error
            };
        default:
            return state;
    }
};

export default moviesReducer;
