import { GET_MENU_DATA_ACTION, MENU_DATA_ERROR_ACTION, MENU_DATA_RECEIVED_ACTION } from '../actions/menu-actions';
import { DEFAULT_KEY, generateCacheTTL } from "redux-cache";

const initialState = {
    [DEFAULT_KEY]: null,
    menu:  {
        itemIds:   [],
        itemsById: null,
        loading:   true,
        error:     null
    },
    title: ''
};

const headerReducer = (state = initialState, action) => {
    switch (action.type) {
        case GET_MENU_DATA_ACTION:
            return {
                ...state,
                menu: {
                    ...state.menu,
                    loading: true,
                    error:   null
                }
            };
        case MENU_DATA_RECEIVED_ACTION:
            return {
                ...state,
                [DEFAULT_KEY]: generateCacheTTL(),
                menu: {
                    ...state.menu,
                    loading:   false,
                    itemIds:   action.payload.menuData.map(item => item.id),
                    itemsById: action.payload.menuData.reduce((carry, element) => {
                        carry[element.id] = element;
                        return carry;
                    }, {})
                },

            };
        case MENU_DATA_ERROR_ACTION:
            return {
                ...state,
                menu: {
                    ...state.menu,
                    loading:   false,
                    error:     action.payload.error,
                    itemIds:   [],
                    itemsById: null
                }

            };
        default:
            return state;
    }
};

export default headerReducer;
