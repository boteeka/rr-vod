import { ASSET_DATA_ERROR_ACTION, ASSET_DATA_RECEIVED_ACTION, GET_ASSET_DATA_ACTION } from "../actions/asset-actions";
import { DEFAULT_KEY } from "redux-cache";

const initialState = {
    [DEFAULT_KEY]: null,
    itemIds:       [],
    itemsById:     {},
    loading:       false,
    error:         null
};
const assetReducer = (state = initialState, action) => {
    switch (action.type) {
        case GET_ASSET_DATA_ACTION:
            return {
                ...state,
                loading: true,
                error:   null
            };
        case ASSET_DATA_RECEIVED_ACTION:
            return {
                ...state,
                loading:   false,
                itemIds:   [...state.itemIds, action.payload.assetData.id],
                itemsById: {
                    ...state.itemsById,
                    [action.payload.assetData.id]: {
                        ...action.payload.assetData,
                        poster_path:   action.payload.assetData.poster_path ? 'https://image.tmdb.org/t/p/original' + action.payload.assetData.poster_path : null,
                        backdrop_path: action.payload.assetData.backdrop_path ? 'https://image.tmdb.org/t/p/original' + action.payload.assetData.backdrop_path : null,
                        production_companies:[
                            ...action.payload.assetData.production_companies.map(production_company => {
                                return {
                                    ...production_company,
                                    logo_path: production_company.logo_path ? 'https://image.tmdb.org/t/p/w500' + production_company.logo_path : null
                                };
                            }),

                        ]
                    }
                }
            };
        case ASSET_DATA_ERROR_ACTION:
            return {
                ...state,
                loading: false,
                error:   action.payload.error,
            };
        default:
            return state;

    }
};

export default assetReducer;
