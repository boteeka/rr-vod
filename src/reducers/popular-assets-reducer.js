import {
    GET_POPULAR_DATA_ACTION,
    POPULAR_DATA_ERROR_ACTION,
    POPULAR_DATA_RECEIVED_ACTION
} from "../actions/popular-assets-actions";
import { DEFAULT_KEY, generateCacheTTL } from "redux-cache";

const initialState = {
    itemIds:   [],
    itemsById: null,
    loading:   true,
    error:     null,
};

const popularAssetsReducer = (state = initialState, action) => {
    let itemIds   = state.itemIds;
    let itemsById = state.itemsById;

    switch (action.type) {
        case GET_POPULAR_DATA_ACTION:
            return {
                ...state,
                loading: true,
                error:   null,
            };
        case POPULAR_DATA_RECEIVED_ACTION:
            itemIds   = action.payload.moviesData.map(item => item.id);
            itemsById = action.payload.moviesData.reduce((carry, element) => {
                carry[element.id] = {
                    ...element,
                    poster_path:   element.poster_path ? 'https://image.tmdb.org/t/p/original' + element.poster_path : null,
                    backdrop_path: element.backdrop_path ? 'https://image.tmdb.org/t/p/original' + element.backdrop_path : null
                };
                return carry;
            }, {});

            return {
                ...state,
                [DEFAULT_KEY]: generateCacheTTL(),
                loading: false,
                itemIds,
                itemsById,
            };
        case POPULAR_DATA_ERROR_ACTION:
            return {
                ...state,
                loading: false,
                error:   action.payload.error
            };
        default:
            return state;
    }
};

export default popularAssetsReducer;
