import {
    CATEGORY_DATA_ERROR_ACTION,
    CATEGORY_DATA_RECEIVED_ACTION,
    GET_CATEGORY_DATA_ACTION
} from "../actions/category-actions";
import { DEFAULT_KEY, generateCacheTTL } from "redux-cache";

const initialState = {
    [DEFAULT_KEY]: null,
    itemIds:       [],
    itemsById:     null,
    loading:       true,
    error:         null
};

const categoryReducer = (state = initialState, action) => {
    switch (action.type) {
        case GET_CATEGORY_DATA_ACTION:
            return {
                ...state,
                loading: true,
                error:   null
            };
        case CATEGORY_DATA_RECEIVED_ACTION:
            return {
                ...state,
                [DEFAULT_KEY]: generateCacheTTL(),
                loading:       false,
                itemIds:       action.payload.categoryData.map(item => item.id),
                itemsById:     action.payload.categoryData.reduce((carry, element) => {
                    carry[element.id] = element;
                    return carry;
                }, {})
            };
        case CATEGORY_DATA_ERROR_ACTION:
            return {
                ...state,
                loading:   false,
                error:     action.payload.error,
                itemIds:   [],
                itemsById: null
            };
        default:
            return state;
    }
};

export default categoryReducer;
