import React, { Component } from "react";
import "./App.css";
import Header from './components/header/Header';
import { connect } from "react-redux";
import CssBaseline from '@material-ui/core/CssBaseline';
import { Route, Switch, withRouter } from "react-router-dom";
import TransitionGroup from "react-transition-group/TransitionGroup";
import CSSTransition from "react-transition-group/CSSTransition";
import Home from "./components/home/Home";
import Categories from "./components/categories/Categories";
import NoMatch from "./components/nomatch/NoMatch";
import Movies from "./components/movies/Movies";
import Asset from "./components/asset/Asset";
import Popular from "./components/popular/Popular";
import theme from './theme/theme';
import MuiThemeProvider from "@material-ui/core/styles/MuiThemeProvider";
import { fetchMenu } from "./actions/menu-actions";

class App extends Component {

    constructor (props) {
        super(props);
        this.state = props.state;
    }

    render () {
        const { location, header: { menu } } = this.props;

        if (menu.loading) {
            return null;
        }

        // Requirements:
        //
        // Download the menus when the application starts
        // - Extend with "static" routes like '/movies/:category_id', '/asset/:id'
        // - Build the routes using the route field of the menu object what you've received with
        //   the request from a

        // Cannot map dynamically received routes without this
        const routeComponentMap = {
            'home':       Home,
            'categories': Categories,
            'popular':    Popular
        };

        return (
            <MuiThemeProvider theme={theme}>
                <CssBaseline/>
                <Header/>
                <section color="primary">
                    <TransitionGroup>
                        <CSSTransition
                            key={location.key}
                            timeout={{ enter: 300, exit: 0 }}
                            classNames={'fade'}
                        >
                            <Switch location={location}>
                                {
                                    menu.itemIds.map(id => (
                                        <Route key={id} exact path={menu.itemsById[id].route}
                                               component={routeComponentMap[id.replace('menu-', '')]}/>
                                    ))
                                }

                                <Route exact path={'/movies/:category_id'} component={Movies}/>
                                <Route exact path={'/asset/:id'} component={Asset}/>

                                <Route component={NoMatch}/>
                            </Switch>
                        </CSSTransition>
                    </TransitionGroup>
                </section>
            </MuiThemeProvider>
        );
    }

    componentDidMount () {
        this.props.dispatch(fetchMenu());
    }
}

const mapStateToProps = function (state) {
    return {
        ...state
    }
};

export default withRouter(connect(mapStateToProps)(App));

