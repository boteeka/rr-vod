import { applyMiddleware, compose, createStore } from "redux";
import thunk from "redux-thunk";
//import logger from 'redux-logger'
import { cacheEnhancer } from 'redux-cache'
import { allReducers } from "../reducers";


const defaultState = {
    header:      {
        menu:  {
            itemIds:   [],
            itemsById: {},
            loading:   true,
            error:     null
        },
        title: 'YAMDB'
    },
    categories:  {
        itemIds:   [],
        itemsById: {},
        loading:   true,
        error:     null
    },
    movies:      {
        itemIds:     [],
        itemsById:   {},
        loading:     true,
        error:       null,
        pagesLoaded: [],
        hasMore:     true
    },
    assets:      {
        itemIds:   [],
        itemsById: null,
        loading:   true,
        error:     null,
    },
    assetVideos: {
        itemIds:   [],
        itemsById: {},
        loading:   true,
        error:     null,
    },
    popular:     {
        itemIds:   [],
        itemsById: {},
        loading:   true,
        error:     null,
    }
};

const middleware = [
    thunk,
    //logger
];

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const store = createStore(
    allReducers,
    defaultState,
    composeEnhancers(
        applyMiddleware(...middleware),
        cacheEnhancer()
    )
);

export default store;
