// import { createMuiTheme } from 'material-ui/styles';
import pink from '@material-ui/core/colors/purple';
import { createMuiTheme } from "@material-ui/core";

const theme = createMuiTheme({
    palette:    {
        type:       'dark',
        primary:    {
            main: 'rgba(11, 9, 29, 0.88)',
        },
        secondary:  pink,
        background: {
            default: '#1a1339',
            paper:   '#1a1339'
        }
    },
    typography: {
        useNextVariants: true,
    },
});

theme.shadows[23] = "0px 11px 14px -7px rgba(255,0,235,0.2),0px 23px 36px 3px rgba(255,0,235,0.14),0px 9px 44px 8px rgba(255,0,235,0.12)";
theme.shadows[16] = "0px 8px 10px -5px rgba(255,0,235,0.2),0px 16px 24px 2px rgba(255,0,235,0.14),0px 6px 30px 5px rgba(255,0,235,0.12)";

export default theme;
