import { fetch_retry } from "../util/fetch-retry";

const GET_MOVIES_DATA_ACTION      = 'movies:getData';
const MOVIES_DATA_RECEIVED_ACTION = 'movies:dataReceived';
const MOVIES_DATA_ERROR_ACTION    = 'movies:error';

const API_ENDPOINT = 'http://online.smartsoft.ro:3333/api/vod/category/:category_id/assets?page=:page_no';

const getMoviesData = (category_id) => {
    return {
        type:    GET_MOVIES_DATA_ACTION,
        payload: { category_id }
    }
};

const moviesDataReceived = (moviesData, category_id) => {
    return {
        type:    MOVIES_DATA_RECEIVED_ACTION,
        payload: { moviesData, category_id }
    }
};

const moviesDataError = (error) => {
    return {
        type:    MOVIES_DATA_ERROR_ACTION,
        payload: { error }
    }
};

export const fetchMovies = (category_id, page = 1) => async (dispatch, getState) => {
    let response;

    dispatch(getMoviesData(category_id));
    try {
        response = await fetch_retry(
            API_ENDPOINT.replace(':category_id', category_id).replace(':page_no', page),
            {},
            3
        );
        if (!response.ok) {
            dispatch(moviesDataError(Error(response.statusText)))
        }
        response = await response.json();
        dispatch(moviesDataReceived(response.data, category_id));
    } catch (error) {
        dispatch(moviesDataError(error))
    }

    return response;
};

export {
    GET_MOVIES_DATA_ACTION,
    MOVIES_DATA_ERROR_ACTION,
    MOVIES_DATA_RECEIVED_ACTION
};
