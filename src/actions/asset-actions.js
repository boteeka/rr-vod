import { checkCacheValid } from "redux-cache";
import { fetch_retry } from "../util/fetch-retry";

const GET_ASSET_DATA_ACTION      = 'asset:getData';
const ASSET_DATA_RECEIVED_ACTION = 'asset:dataReceived';
const ASSET_DATA_ERROR_ACTION    = 'asset:error';

const API_ENDPOINT = 'http://online.smartsoft.ro:3333/api/vod/asset/:asset_id';

const getAssetData = () => {
    return {
        type: GET_ASSET_DATA_ACTION,
    }
};

const assetDataReceived = (assetData) => {
    return {
        type:    ASSET_DATA_RECEIVED_ACTION,
        payload: { assetData }
    }
};

const assetDataError = (error) => {
    return {
        type:    ASSET_DATA_ERROR_ACTION,
        payload: { error }
    }
};

export const fetchAsset = (asset_id) => async (dispatch, getState) => {
    const isCacheValid = checkCacheValid(getState, "assets");
    if (isCacheValid) {
        return null;
    }

    let response;

    dispatch(getAssetData());
    try {
        response = await fetch_retry(API_ENDPOINT.replace(':asset_id', asset_id), {}, 3);
        if (!response.ok) {
            dispatch(assetDataError(Error(response.statusText)))
        }
        response = await response.json();
        dispatch(assetDataReceived(response.data));
    } catch (error) {
        dispatch(assetDataError(error))
    }

    return response;
};

export {
    GET_ASSET_DATA_ACTION,
    ASSET_DATA_ERROR_ACTION,
    ASSET_DATA_RECEIVED_ACTION
};
