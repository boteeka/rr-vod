import { checkCacheValid } from "redux-cache";
import { fetch_retry } from "../util/fetch-retry";

const GET_POPULAR_DATA_ACTION      = 'popular:getData';
const POPULAR_DATA_RECEIVED_ACTION = 'popular:dataReceived';
const POPULAR_DATA_ERROR_ACTION    = 'popular:error';

const API_ENDPOINT = 'http://online.smartsoft.ro:3333/api/vod/popular';

const getPopularData = () => {
    return {
        type: GET_POPULAR_DATA_ACTION,
    }
};

const popularDataReceived = (moviesData) => {
    return {
        type:    POPULAR_DATA_RECEIVED_ACTION,
        payload: { moviesData }
    }
};

const popularDataError = (error) => {
    return {
        type:    POPULAR_DATA_ERROR_ACTION,
        payload: { error }
    }
};

export const fetchPopularAssets = () => async (dispatch, getState) => {
    const isCacheValid = checkCacheValid(getState, "popular");
    if (isCacheValid) {
        return null;
    }

    let response;

    dispatch(getPopularData());
    try {
        response = await fetch_retry(
            API_ENDPOINT,
            {},
            3
        );
        if (!response.ok) {
            dispatch(popularDataError(Error(response.statusText)))
        }
        response = await response.json();
        dispatch(popularDataReceived(response.data));
    } catch (error) {
        dispatch(popularDataError(error))
    }

    return response;
};

export { GET_POPULAR_DATA_ACTION, POPULAR_DATA_ERROR_ACTION, POPULAR_DATA_RECEIVED_ACTION };
