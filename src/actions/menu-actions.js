import { checkCacheValid } from "redux-cache";
import { fetch_retry } from "../util/fetch-retry";

const GET_MENU_DATA_ACTION      = 'menu:getData';
const MENU_DATA_RECEIVED_ACTION = 'menu:dataReceived';
const MENU_DATA_ERROR_ACTION    = 'menu:error';

const API_ENDPOINT = 'http://online.smartsoft.ro:3333/api/static/menu';

const getMenuData = () => {
    return {
        type: GET_MENU_DATA_ACTION,
    }
};

const menuDataReceived = (menuData) => {
    return {
        type:    MENU_DATA_RECEIVED_ACTION,
        payload: { menuData }
    }
};

const menuDataError = (error) => {
    return {
        type:    MENU_DATA_ERROR_ACTION,
        payload: { error }
    }
};

export const fetchMenu = () => async (dispatch, getState) => {
    const isCacheValid = checkCacheValid(getState, "header");
    if (isCacheValid) {
        return null;
    }

    let response;

    dispatch(getMenuData());
    try {
        response = await fetch_retry(API_ENDPOINT, {}, 3);
        if (!response.ok) {
            dispatch(menuDataError(Error(response.statusText)))
        }
        response = await response.json();
        dispatch(menuDataReceived(response.data));
    } catch (error) {
        dispatch(menuDataError(error))
    }

    return response;
};

export {
    GET_MENU_DATA_ACTION,
    MENU_DATA_ERROR_ACTION,
    MENU_DATA_RECEIVED_ACTION
};
