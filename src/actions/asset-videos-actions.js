import { fetch_retry } from "../util/fetch-retry";

const GET_ASSETVIDEO_DATA_ACTION      = 'assetvideo:getData';
const ASSETVIDEO_DATA_RECEIVED_ACTION = 'assetvideo:dataReceived';
const ASSETVIDEO_DATA_ERROR_ACTION    = 'assetvideo:error';


const API_ENDPOINT = 'http://online.smartsoft.ro:3333/api/vod/asset/:asset_id/videos';

const getAssetvideoData = () => {
    return {
        type: GET_ASSETVIDEO_DATA_ACTION,
    }
};

const assetvideoDataReceived = (assetvideoData) => {
    return {
        type:    ASSETVIDEO_DATA_RECEIVED_ACTION,
        payload: { assetvideoData }
    }
};

const assetvideoDataError = (error) => {
    return {
        type:    ASSETVIDEO_DATA_ERROR_ACTION,
        payload: { error }
    }
};

export const fetchAssetvideos = (asset_id) => async (dispatch, getState) => {
    let response;

    dispatch(getAssetvideoData());
    try {
        response = await fetch_retry(API_ENDPOINT.replace(':asset_id', asset_id), {}, 3);
        if (!response.ok) {
            dispatch(assetvideoDataError(Error(response.statusText)))
        }
        response = await response.json();
        dispatch(assetvideoDataReceived(response.data));
    } catch (error) {
        dispatch(assetvideoDataError(error))
    }

    return response;
};

export {
    GET_ASSETVIDEO_DATA_ACTION,
    ASSETVIDEO_DATA_ERROR_ACTION,
    ASSETVIDEO_DATA_RECEIVED_ACTION
};
