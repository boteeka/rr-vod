import { checkCacheValid } from "redux-cache";
import { fetch_retry } from "../util/fetch-retry";

const GET_CATEGORY_DATA_ACTION      = 'category:getData';
const CATEGORY_DATA_RECEIVED_ACTION = 'category:dataReceived';
const CATEGORY_DATA_ERROR_ACTION    = 'category:error';

const API_ENDPOINT = 'http://online.smartsoft.ro:3333/api/vod/category';

const getCategoryData = () => {
    return {
        type: GET_CATEGORY_DATA_ACTION,
    }
};

const categoryDataReceived = (categoryData) => {
    return {
        type:    CATEGORY_DATA_RECEIVED_ACTION,
        payload: { categoryData }
    }
};

const categoryDataError = (error) => {
    return {
        type:    CATEGORY_DATA_ERROR_ACTION,
        payload: { error }
    }
};

export const fetchCategories = () => async (dispatch, getState) => {
    const isCacheValid = checkCacheValid(getState, "categories");
    if (isCacheValid) {
        return null;
    }

    let response;

    dispatch(getCategoryData());
    try {
        response = await fetch_retry(API_ENDPOINT, {}, 3);
        if (!response.ok) {
            dispatch(categoryDataError(Error(response.statusText)))
        }
        response = await response.json();
        dispatch(categoryDataReceived(response.data.genres));
    } catch (error) {
        dispatch(categoryDataError(error))
    }

    return response;
};

export {
    GET_CATEGORY_DATA_ACTION,
    CATEGORY_DATA_RECEIVED_ACTION,
    CATEGORY_DATA_ERROR_ACTION
};
